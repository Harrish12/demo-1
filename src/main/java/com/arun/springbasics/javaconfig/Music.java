package com.arun.springbasics.javaconfig;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class Music {

	@Value("Darde-Disco")
	String name;
	
	@Value("sukhvinder singh")
	String singerName;
	
	public Music() {
		System.out.println(this.getClass().getSimpleName());
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSingerName() {
		return singerName;
	}

	public void setSingerName(String singerName) {
		this.singerName = singerName;
	}

	@Override
	public String toString() {
		return "Music [name=" + name + ", singerName=" + singerName + "]";
	}
	
	
}
